# OutFoxed
OutFoxed is a game I developed for my Service Learning Project at Clark College. Currently it is in an early alpha build, mostly for demonstrating basic character movement and physics interactions between the player character and the world around them via grappling hook. Swing around 2D levels collecting coins, finding secrets, and outfoxing gravity.

## Controls
A/D - Left/Right

Space - Jump

M1 - Grapple

## Installation
Download and unzip OutFoxed.zip, then run OutFoxed.exe

## Support
bobbysenpai on Discord

## Authors and acknowledgment
[Kenney's free game assets](https://kenney.nl/assets)

## Project status
In development, but reliant on personal free time so dev speed will fluctuate.